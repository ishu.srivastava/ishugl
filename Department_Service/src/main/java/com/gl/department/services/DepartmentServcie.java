package com.gl.department.services;

import com.gl.department.entity.Department;
import com.gl.department.error.DepartmentException;

import java.util.List;

public interface DepartmentServcie {
    Department createDepartment(Department department);


    Department findDepartmentById(Long departmentId) throws DepartmentException;

    void deleteById(Long departmentId);

    Department updateById(Long departmentId, Department department);

    List<Department> GetAllDepartment();
}
