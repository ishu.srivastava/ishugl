package com.gl.department.controller;

import com.gl.department.entity.Department;
import com.gl.department.error.DepartmentException;
import com.gl.department.services.DepartmentServcie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Dep")
public class DepartmentController {

    @Autowired
    private DepartmentServcie depser;


    @PostMapping("/Crt")
    public Department createDepartment(@RequestBody Department department){
        return depser.createDepartment(department);
    }

    @GetMapping("/GetAll")
    public List<Department> GetAllDepartment(){
        return depser.GetAllDepartment();
    }

    @GetMapping("/{id}/Get")
    public Department findDepartmentById(@PathVariable("id") Long departmentId) throws DepartmentException {
        return depser.findDepartmentById(departmentId);
    }

    @DeleteMapping("/{id}/delete")
    public String deleteById(@PathVariable("id") Long departmentId){
        depser.deleteById(departmentId);
        return "Data is deleted Successfully";
    }

    @PutMapping("/{id}/update")
    public Department updateById(@PathVariable("id") Long departmentId
            ,@RequestBody Department department){
        return depser.updateById(departmentId,department);
    }


}
