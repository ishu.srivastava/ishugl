package com.gl.user.service;

import com.gl.user.VO.ResponseTemplateVO;
import com.gl.user.entity.User;

public interface UserService {

    User createUser(User user);


    ResponseTemplateVO getUserWithDepartment(Long userId);
}
